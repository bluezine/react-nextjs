'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Layout = require('../components/Layout');

var _Layout2 = _interopRequireDefault(_Layout);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = 'D:\\Repository\\next-quick-start\\pages\\ssr-test.js?entry';


var SSRTest = function (_React$Component) {
    (0, _inherits3.default)(SSRTest, _React$Component);

    function SSRTest() {
        (0, _classCallCheck3.default)(this, SSRTest);

        return (0, _possibleConstructorReturn3.default)(this, (SSRTest.__proto__ || (0, _getPrototypeOf2.default)(SSRTest)).apply(this, arguments));
    }

    (0, _createClass3.default)(SSRTest, [{
        key: 'render',

        /*render() {
            return (
                <Layout>
                    {this.props.from} 에서 실행이 되었어요.
                </Layout>
            );
        }*/
        value: function render() {
            var users = this.props.users;

            var userList = users.map(function (user) {
                return _react2.default.createElement('li', { key: user.id, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 27
                    }
                }, user.username);
            });

            return _react2.default.createElement(_Layout2.default, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 31
                }
            }, _react2.default.createElement('ul', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 32
                }
            }, userList));
        }
    }], [{
        key: 'getInitialProps',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref) {
                var req = _ref.req;
                var response;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return _axios2.default.get('https://jsonplaceholder.typicode.com/users');

                            case 2:
                                response = _context.sent;
                                return _context.abrupt('return', {
                                    users: response.data
                                });

                            case 4:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function getInitialProps(_x) {
                return _ref2.apply(this, arguments);
            }

            return getInitialProps;
        }()
    }]);

    return SSRTest;
}(_react2.default.Component);

exports.default = SSRTest;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzXFxzc3ItdGVzdC5qcyJdLCJuYW1lcyI6WyJMYXlvdXQiLCJheGlvcyIsIlNTUlRlc3QiLCJ1c2VycyIsInByb3BzIiwidXNlckxpc3QiLCJtYXAiLCJ1c2VyIiwiaWQiLCJ1c2VybmFtZSIsInJlcSIsImdldCIsInJlc3BvbnNlIiwiZGF0YSIsIlJlYWN0IiwiQ29tcG9uZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLEFBQU8sQUFBWTs7OztBQUNuQixBQUFPOzs7Ozs7Ozs7SSxBQUVEOzs7Ozs7Ozs7O2FBWUY7Ozs7Ozs7OztpQ0FPUztnQkFBQSxBQUNHLFFBQVUsS0FEYixBQUNrQixNQURsQixBQUNHLEFBRVI7O2dCQUFNLGlCQUFXLEFBQU0sSUFDbkIsZ0JBQUE7dUNBQVEsY0FBQSxRQUFJLEtBQUssS0FBVCxBQUFjO2tDQUFkO29DQUFBLEFBQW1CO0FBQW5CO2lCQUFBLE9BQVIsQUFBUSxBQUF3QjtBQURwQyxBQUFpQixBQUlqQixhQUppQjs7bUNBS2IsQUFBQzs7OEJBQUQ7Z0NBQUEsQUFDSTtBQURKO0FBQUEsYUFBQSxrQkFDSSxjQUFBOzs4QkFBQTtnQ0FBQSxBQUNLO0FBREw7QUFBQSxlQUZSLEFBQ0ksQUFDSSxBQUtYOzs7Ozs7b0JBaEM4QixBLFdBQUEsQTs7Ozs7Ozt1Q0FJSixnQkFBQSxBQUFNLElBQU4sQSxBQUFVOztpQ0FBM0I7QTs7MkNBRUssUyxBQURKLEFBQ2E7QUFEYixBQUNIOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBUFUsZ0JBQU0sQSxBQW9DNUI7O2tCQUFBLEFBQWUiLCJmaWxlIjoic3NyLXRlc3QuanM/ZW50cnkiLCJzb3VyY2VSb290IjoiRDovUmVwb3NpdG9yeS9uZXh0LXF1aWNrLXN0YXJ0In0=