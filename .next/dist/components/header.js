'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _link = require('next\\dist\\lib\\link.js');

var _link2 = _interopRequireDefault(_link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = 'D:\\Repository\\next-quick-start\\components\\header.js';


var linkStyle = {
    marginRight: '1rem'
};
var Header = function Header() {
    return _react2.default.createElement('div', {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 8
        }
    }, _react2.default.createElement(_link2.default, { href: '/', __source: {
            fileName: _jsxFileName,
            lineNumber: 9
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 9
        }
    }, '\uBA54\uC778')), _react2.default.createElement(_link2.default, { href: '/about', __source: {
            fileName: _jsxFileName,
            lineNumber: 10
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 10
        }
    }, '\uD3F0\uD2B8\uB458\uB7EC\uBCF4\uAE30')), _react2.default.createElement(_link2.default, { href: '/product', __source: {
            fileName: _jsxFileName,
            lineNumber: 11
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 11
        }
    }, '\uC11C\uBE44\uC2A4\uC0C1\uD488')), _react2.default.createElement(_link2.default, { href: '/guide', __source: {
            fileName: _jsxFileName,
            lineNumber: 12
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 12
        }
    }, '\uC774\uC6A9\uC548\uB0B4')), _react2.default.createElement(_link2.default, { href: '/support', __source: {
            fileName: _jsxFileName,
            lineNumber: 13
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 13
        }
    }, '\uC11C\uBE44\uC2A4\uC9C0\uC6D0')), _react2.default.createElement(_link2.default, { href: '/gallery', __source: {
            fileName: _jsxFileName,
            lineNumber: 14
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 14
        }
    }, '\uC6F9\uD3F0\uD2B8\uC801\uC6A9\uC0AC\uB840')), _react2.default.createElement(_link2.default, { href: '/search?keyword=test', __source: {
            fileName: _jsxFileName,
            lineNumber: 16
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 16
        }
    }, '\uD3F0\uD2B8\uBA85\uAC80\uC0C9')), _react2.default.createElement(_link2.default, { href: '/ssr-test', __source: {
            fileName: _jsxFileName,
            lineNumber: 17
        }
    }, _react2.default.createElement('a', { style: linkStyle, __source: {
            fileName: _jsxFileName,
            lineNumber: 17
        }
    }, 'SSR \uD14C\uC2A4\uD2B8')));
};

exports.default = Header;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHNcXGhlYWRlci5qcyJdLCJuYW1lcyI6WyJMaW5rIiwibGlua1N0eWxlIiwibWFyZ2luUmlnaHQiLCJIZWFkZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxBQUFPOzs7Ozs7Ozs7QUFFUCxJQUFNO2lCQUFOLEFBQWtCLEFBQ0Q7QUFEQyxBQUNkO0FBRUosSUFBTSxTQUFTLFNBQVQsQUFBUyxTQUFNLEFBQ2pCOzJCQUNJLGNBQUE7O3NCQUFBO3dCQUFBLEFBQ0k7QUFESjtBQUFBLEtBQUEsa0JBQ0ksQUFBQyxnQ0FBSyxNQUFOLEFBQVc7c0JBQVg7d0JBQUEsQUFBZTtBQUFmO3VCQUFlLGNBQUEsT0FBRyxPQUFILEFBQVU7c0JBQVY7d0JBQUE7QUFBQTtPQURuQixBQUNJLEFBQWUsQUFDZixrQ0FBQSxBQUFDLGdDQUFLLE1BQU4sQUFBVztzQkFBWDt3QkFBQSxBQUFvQjtBQUFwQjt1QkFBb0IsY0FBQSxPQUFHLE9BQUgsQUFBVTtzQkFBVjt3QkFBQTtBQUFBO09BRnhCLEFBRUksQUFBb0IsQUFDcEIsMERBQUEsQUFBQyxnQ0FBSyxNQUFOLEFBQVc7c0JBQVg7d0JBQUEsQUFBc0I7QUFBdEI7dUJBQXNCLGNBQUEsT0FBRyxPQUFILEFBQVU7c0JBQVY7d0JBQUE7QUFBQTtPQUgxQixBQUdJLEFBQXNCLEFBQ3RCLG9EQUFBLEFBQUMsZ0NBQUssTUFBTixBQUFXO3NCQUFYO3dCQUFBLEFBQW9CO0FBQXBCO3VCQUFvQixjQUFBLE9BQUcsT0FBSCxBQUFVO3NCQUFWO3dCQUFBO0FBQUE7T0FKeEIsQUFJSSxBQUFvQixBQUNwQiw4Q0FBQSxBQUFDLGdDQUFLLE1BQU4sQUFBVztzQkFBWDt3QkFBQSxBQUFzQjtBQUF0Qjt1QkFBc0IsY0FBQSxPQUFHLE9BQUgsQUFBVTtzQkFBVjt3QkFBQTtBQUFBO09BTDFCLEFBS0ksQUFBc0IsQUFDdEIsb0RBQUEsQUFBQyxnQ0FBSyxNQUFOLEFBQVc7c0JBQVg7d0JBQUEsQUFBc0I7QUFBdEI7dUJBQXNCLGNBQUEsT0FBRyxPQUFILEFBQVU7c0JBQVY7d0JBQUE7QUFBQTtPQU4xQixBQU1JLEFBQXNCLEFBRXRCLGdFQUFBLEFBQUMsZ0NBQUssTUFBTixBQUFXO3NCQUFYO3dCQUFBLEFBQWtDO0FBQWxDO3VCQUFrQyxjQUFBLE9BQUcsT0FBSCxBQUFVO3NCQUFWO3dCQUFBO0FBQUE7T0FSdEMsQUFRSSxBQUFrQyxBQUNsQyxvREFBQSxBQUFDLGdDQUFLLE1BQU4sQUFBVztzQkFBWDt3QkFBQSxBQUF1QjtBQUF2Qjt1QkFBdUIsY0FBQSxPQUFHLE9BQUgsQUFBVTtzQkFBVjt3QkFBQTtBQUFBO09BVi9CLEFBQ0ksQUFTSSxBQUF1QixBQUdsQztBQWRELEFBZ0JBOztrQkFBQSxBQUFlIiwiZmlsZSI6ImhlYWRlci5qcyIsInNvdXJjZVJvb3QiOiJEOi9SZXBvc2l0b3J5L25leHQtcXVpY2stc3RhcnQifQ==