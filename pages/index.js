import Link from 'next/link'
import Layout from '../components/Layout';
import Head from 'next/head'; 

const Index = () => (
    <Layout>
        <style jsx> 
            {` 
                h1 {
                    color:green
                }
                .bx-wrapper .bx-pager {
                    bottom: -95px;
                }
            
                    .bx-wrapper .bx-pager a {
                        display: none;
                        
                    }
            
                        .bx-wrapper .bx-pager a:hover,
                        .bx-wrapper .bx-pager a.active {
                            border: solid #5280DD 1px;
                        }
            
                .bx-wrapper {
                    margin-bottom: 0;
                }
                #main-news .bx-pager {
                    display:none; 
                }
            `}
        </style> 
        <Head> 
            <title> 
             웹폰트 서비스 : 타이포링크 
            </title>
            <link rel="stylesheet" type="text/css" href="http://typolink.co.kr/public/css/site.less" />
            <link rel="SHORTCUT ICON" href="http://typolink.co.kr/favicon.ico"/>

            <meta charset="utf-8" />
            <meta name="msapplication-config" content="none"/>

            <meta http-equiv="description" content="1,000개 이상의 웹폰트를 쉽게 쓸 수 있는 스마트한 서비스 타이포링크, 웹접근성과 웹표준에 필수적입니다. 이제 올바른 웹을 구현하세요." />
            <meta http-equiv="keyword" content="웹폰트,폰트, 캘리그라피, 켈리그라피, 글씨체, 서체, 글꼴, font, typo, 한글, 한글글씨체, 타이포그래픽, 타이포그라피, 타이포그래피, 타이포, 무료폰트, 유료폰트, 한글폰트, 한글글꼴, 영어폰트, 영문폰트, 영어글꼴, 영문글꼴, 클라우드폰트, 폰트모음, 폰트추천, 예쁜폰트, 한글폰트구매,  고딕, 고딕체, 명조, 명조체, 손글씨, 손글씨체, 필기체, 조형체, 옛글체, 붓글씨체, 붓글씨폰트,  본문체, 힌팅폰트, 디지털폰트, 웹폰트적용, 구글웹폰트,  접근성, 웹접근성, 반응형웹, 웹표준,  폰트릭스, 릭스폰트, 세종폰트, 나눔고딕, 나눔명조, 나눔글꼴, 나눔폰트, 네이버폰트, 헤움, 헤움디자인, 우리글닷컴, 아시아폰트, 아시아소프트, 좋은글씨, 더폰트그룹, 디엑스코리아, DX코리아, 훈디자인" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            
        </Head>

        <section id="main-top">
            <div class="wrap">
                <h1>웹을 바꾸다.</h1>
                <p>Typolink for smart web</p>
            </div>
        </section>
        
        <section id="main-why">
            <div class="wrap">
                <section class="heading">
                    <h1>왜 타이포링크인가?</h1>
                </section>
                <div class="slider">
                    <div class="item item01">
                        <h2>1,000개 이상의 웹폰트가 있습니다.</h2>
                        <p>
                            타이포링크의 웹폰트로 웹사이트의 디자인은 풍성해 집니다. <br />
                            획일적인 폰트 사용에서 벗어나 웹사이트의 컨셉에 맞는 폰트를 사용하실 수 있습니다. <br />
                            타이포링크의 폰트는 계속 추가됩니다.
                        </p>
                    </div>
                    <div class="item item02">
                        <h2>사용이 쉽습니다.</h2>
                        <p>
                            누구나 쉽게 웹폰트를 쓰고 바꿀 수 있습니다. <br />
                            몇 줄의 코드 입력만으로 다양한 브라우저와 최신 디바이스에 대응되고 합리적인 가격과 <br />
                            간단한 사용법으로 운영비용이 절감됩니다.
                        </p>
                    </div>
                    <div class="item item03">
                        <h2>빠르고 안정적입니다.</h2>
                        <p>
                            독자적인 폰트압축기술로 폰트의 용량을 획기적으로 줄였습니다. <br />
                            폰트서버에 CDN(contents delivery network)을 적용하여 웹폰트의 전송을 <br />
                            더욱 빠르고 안정적으로 유지합니다.
                        </p>
                    </div>
                    <div class="item item04">
                        <h2>웹접근성과 SEO효과를 높힙니다 </h2>
                        <p>
                            그래픽텍스트가 'DB화' 되어 사이트의 '웹접근성'과 'SEO효과'를 높힐 수 있습니다.<br />
                            100% 웹표준 HTML과 CSS를 지원하고 다양한 디바이스 최적화를 위한 <br />
                            반응형웹과 모바일웹 구현에도 효과적입니다.
                        </p>
                    </div>
                </div>
            </div>
        </section>


        <section id="main-use">
            <div class="wrap">
                <div class="item_use">
                    <h1 class="tit">이용방법</h1>
                    <ol>
                        <li>
                            <img src="/public/images/blank.png" class="item_img1" alt="" />
                            <p>원하는 서비스 상품을<br />선택하여 신청!</p>
                        </li>
                        <li>
                            <img src="/public/images/blank.png" class="item_img2" alt="" />
                            <p>검색필터를 활용하여<br />폰트를 검색!</p>
                        </li>
                        <li>
                            <img src="/public/images/blank.png" class="item_img3" alt="" />
                            <p>선택한 폰트의 코드를<br />웹페이지에 적용!</p>
                        </li>
                        <li>
                            <img src="/public/images/blank.png" class="item_img4" alt="" />
                            <p>웹사이트의 도메인을 <br />등록하여 반영!</p>
                        </li>
                    </ol>
                </div>
            </div>
        </section>
        <section id="main-support">
            <div class="wrap">
                <dl>
                    <dt>폰트 디자이너이신가요?</dt>
                    <dd>
                        직접 제작하신 폰트를 웹폰트로 서비스해 보세요.<br />
                        폰트 디자이너분들의 크리에이티브한 폰트들을 기다리고<br />
                        있습니다. 서비스에 참여를 원하시면 <Link href="/">여기로 </Link>
                    </dd>
                </dl>
                <dl>
                    <dt>업무제휴를 원하시나요?</dt>
                    <dd>
                        폭넓은 협력을 통해 성공적인 파트너십을<br />
                        구축합니다. 다양한 형태의 업무제휴에 대한<br />
                        문의는 <Link href="/">여기로 </Link>
                    </dd>
                </dl>
                <dl class="item_last">
                    <dt>더 많은 도움이 필요하신가요?</dt>
                    <dd>
                        <p><img src="/public/images/blank.png" class="mapping mail" alt="" /> 이메일을 보내시려면 <Link href="/">여기로 </Link></p>
                    </dd>
                </dl>
            </div>
        </section>

    </Layout>
);

export default Index;