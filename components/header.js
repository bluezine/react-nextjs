import Link from 'next/link';

const linkStyle = {
    marginRight: '1rem'
}
const Header = () => {
    return (
        <div>
            <Link href="/"><a style={linkStyle}>메인</a></Link>
            <Link href="/about"><a style={linkStyle}>폰트둘러보기</a></Link>
            <Link href="/product"><a style={linkStyle}>서비스상품</a></Link>
            <Link href="/guide"><a style={linkStyle}>이용안내</a></Link>
            <Link href="/support"><a style={linkStyle}>서비스지원</a></Link>
            <Link href="/gallery"><a style={linkStyle}>웹폰트적용사례</a></Link>
            
            <Link href="/search?keyword=test"><a style={linkStyle}>폰트명검색</a></Link>
            <Link href="/ssr-test"><a style={linkStyle}>SSR 테스트</a></Link>
        </div>
    );
};

export default Header;